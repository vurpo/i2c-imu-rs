use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;
use nalgebra::Vector3;

const I2C_ADDRESS: u8 = 0x53;

const EARTH_GRAVITY_MS2: f32 = 9.80665;
const SCALE_MULTIPLIER: f32 = 0.004;

const REG_DATA_FORMAT: u8 = 0x31;
const REG_BW_RATE: u8 = 0x2C;
const REG_POWER_CTL: u8 = 0x2D;

const REG_X: u8 = 0x32;
const REG_Y: u8 = 0x34;
const REG_Z: u8 = 0x36;

const MEASURE: u8 = 0x08;

pub enum BandwidthRate {
  Rate1600Hz = 0x0F,
   Rate800Hz = 0x0E,
   Rate400Hz = 0x0D,
   Rate200Hz = 0x0C,
   Rate100Hz = 0x0B,
    Rate50Hz = 0x0A,
    Rate25Hz = 0x09,
}

pub enum Range {
  Range2G  = 0x00,
  Range4G  = 0x01,
  Range8G  = 0x02,
  Range16G = 0x03,
}

pub struct ADXL345 {
  bus: LinuxI2CDevice,
  address: u8
}

impl ADXL345 {
  pub fn new() -> Result<ADXL345, ()> {
    let mut bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let mut sensor = ADXL345 { bus: bus, address: I2C_ADDRESS };
    sensor.set_bandwidth_rate(BandwidthRate::Rate100Hz);
    sensor.set_range(Range::Range2G);
    sensor.enable_measurement();
    Ok(sensor)
  }

  pub fn enable_measurement(&mut self) -> Result<(), ()> {
    self.bus.smbus_write_byte_data(REG_POWER_CTL, MEASURE).map_err(|_|())
  }

  pub fn set_bandwidth_rate(&mut self, rate: BandwidthRate) -> Result<(), ()> {
    self.bus.smbus_write_byte_data(REG_BW_RATE, rate as u8).map_err(|_|())
  }

  pub fn set_range(&mut self, range: Range) -> Result<(), ()> {
    let mut value = try!(self.bus.smbus_read_byte_data(REG_DATA_FORMAT).map_err(|_|()));
    value = value & !0x0F;
    value = value | (range as u8);
    value = value | 0x08;

    self.bus.smbus_write_byte_data(REG_DATA_FORMAT, value).map_err(|_|())
  }

  pub fn get_axes_raw(&mut self) -> Result<[i16; 3], ()> {
    let x = try!(self.bus.smbus_read_word_data(REG_X).map_err(|_|())) as i16;
    let y = try!(self.bus.smbus_read_word_data(REG_Y).map_err(|_|())) as i16;
    let z = try!(self.bus.smbus_read_word_data(REG_Z).map_err(|_|())) as i16;
    
    Ok([x,y,z])
  }
  
  pub fn get_axes_g(&mut self) -> Result<Vector3<f32>, ()> {
    let values = try!(self.get_axes_raw());
    Ok(Vector3::new(values[0] as f32 *SCALE_MULTIPLIER, values[1] as f32 *SCALE_MULTIPLIER, values[2] as f32 *SCALE_MULTIPLIER))
  }

  pub fn get_axes_ms2(&mut self) -> Result<Vector3<f32>, ()> {
    Ok(try!(self.get_axes_g())*EARTH_GRAVITY_MS2)
  }
}
