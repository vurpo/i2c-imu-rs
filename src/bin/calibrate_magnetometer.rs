extern crate i2c_imu;
extern crate rustc_serialize;
extern crate ctrlc;

use i2c_imu::{HMC5883L, MagnetometerCalibration};
use std::cmp::{max,min};
use std::thread;
use std::io::stdout;
use std::sync::mpsc::channel;
use rustc_serialize::json;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;

fn main() {
  let (send, recv) = channel::<()>();

  ctrlc::set_handler(move || { send.send(()); });
  
  let mut mag = HMC5883L::new().unwrap();

  println!("Determining magnetometer hard-iron error. Turn device in all directions.");

  let mut max_values: [i16; 3] = [0; 3];
  let mut min_values: [i16; 3] = [0; 3];
  let mut avg_values: [i16; 3];

  loop {
    let values = mag.get_axes_raw().unwrap();

    max_values = [max(max_values[0], values[0]), max(max_values[1], values[1]), max(max_values[2], values[2])];
    min_values = [min(min_values[0], values[0]), min(min_values[1], values[1]), min(min_values[2], values[2])];
    avg_values = [max_values[0]/2 + min_values[0]/2, max_values[1]/2 + min_values[1]/2, max_values[2]/2 + min_values[2]/2];

    print!("Current hard-iron error: {:+5} {:+5} {:+5}\r", avg_values[0], avg_values[1], avg_values[2]);
    stdout().flush().unwrap(); // Stdout is line-buffered, so here it needs to be explicitly flushed

    if let Ok(_) = recv.try_recv() { println!(""); break; }

    thread::sleep_ms(100);
  }
  
  let calibration_data = json::encode(&MagnetometerCalibration{ hard_iron: avg_values }).unwrap();
  if let Ok(mut file) = File::create("/home/pi/magnetometer_calibration.json") {
    file.write_all(calibration_data.as_bytes()).unwrap();
    println!("Wrote magnetometer calibration data to /home/pi/magnetometer-calibration.json.");
  }
}
