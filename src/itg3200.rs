use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;
use nalgebra::Vector3;

const I2C_ADDRESS: u8 = 0x68;

const LSB_PER_DEGREE: f32 = 14.375; // Source: datasheet
const LSB_PER_RADIAN: f32 = 823.6;  // calculated from LSB/degree

const REG_PWR_MGM: u8    = 0x3E;
const REG_SMPLRT_DIV: u8 = 0x15;
const REG_DLPF_FS: u8    = 0x16;
const REG_INT_CFG: u8    = 0x17;

const REG_X: u8 = 0x1D;
const REG_Y: u8 = 0x1F;
const REG_Z: u8 = 0x21;

pub struct ITG3200 {
  bus: LinuxI2CDevice,
  address: u8,
}

impl ITG3200 {
  pub fn new() -> Result<ITG3200, ()> {
    let mut bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let mut sensor = ITG3200 { bus: bus, address: I2C_ADDRESS };

    try!(sensor.bus.smbus_write_byte_data(REG_PWR_MGM, 0x01).map_err(|_|()));      // Set clock source to X gyro (recommended)
    try!(sensor.bus.smbus_write_byte_data(REG_SMPLRT_DIV, 0x07).map_err(|_|()));   // Samplerate divider = 125Hz
    try!(sensor.bus.smbus_write_byte_data(REG_DLPF_FS, 0x18|0x01).map_err(|_|())); // Set range and lowpass filter
    try!(sensor.bus.smbus_write_byte_data(REG_INT_CFG, 0x00).map_err(|_|()));      // Disable interrupt pin

    Ok(sensor)
  }

  pub fn get_axes_raw(&mut self) -> Result<[i16; 3], ()> {
    let x = try!(self.bus.smbus_read_word_data(REG_X).map_err(|_|()));
    let y = try!(self.bus.smbus_read_word_data(REG_Y).map_err(|_|()));
    let z = try!(self.bus.smbus_read_word_data(REG_Z).map_err(|_|()));

    let x = (x>>8) | (x<<8);
    let y = (y>>8) | (y<<8);
    let z = (z>>8) | (z<<8);

    Ok([x as i16, y as i16, z as i16])
  }

  pub fn get_axes_rad(&mut self) -> Result<Vector3<f32>, ()> {
    let values = try!(self.get_axes_raw());

    Ok(Vector3::new(values[0] as f32 / LSB_PER_RADIAN, values[1] as f32 / LSB_PER_RADIAN, values[2] as f32 / LSB_PER_RADIAN))
  }

  pub fn get_axes_deg(&mut self) -> Result<Vector3<f32>, ()> {
    let values = try!(self.get_axes_raw());

    Ok(Vector3::new(values[0] as f32 / LSB_PER_DEGREE, values[1] as f32 / LSB_PER_DEGREE, values[2] as f32 / LSB_PER_DEGREE))
  }

}
