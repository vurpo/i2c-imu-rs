extern crate i2cdev;
extern crate nalgebra;
extern crate rustc_serialize;
extern crate ctrlc;

mod adxl345;
mod itg3200;
mod hmc5883l;

pub use adxl345::ADXL345;
pub use itg3200::ITG3200;
pub use hmc5883l::{HMC5883L, MagnetometerCalibration};
