use i2cdev::linux::LinuxI2CDevice;
use i2cdev::core::I2CDevice;
use nalgebra::Vector3;
use std::fs::File;
use std::path::Path;
use std::io::prelude::*;
use rustc_serialize::json;

const I2C_ADDRESS: u8 = 0x1E;

const REG_CONF_A: u8 = 0;
const REG_CONF_B: u8 = 1;
const REG_MODE: u8   = 2;

const REG_X: u8  = 3;
const REG_Y: u8  = 7;
const REG_Z: u8  = 5;  // Word

#[derive(RustcEncodable,RustcDecodable)]
pub struct MagnetometerCalibration {
  pub hard_iron: [i16; 3],
}

impl MagnetometerCalibration {
  pub fn get_calibration() -> Result<MagnetometerCalibration, ()> {
    let mut contents: String = String::new();
    if let Ok(mut file) = File::open("/home/pi/magnetometer_calibration.json") {
      if let Ok(_) = file.read_to_string(&mut contents) {
        if let Ok(decoded) = json::decode::<MagnetometerCalibration>(&contents) {
          return Ok(MagnetometerCalibration{hard_iron: decoded.hard_iron});
        } else { return Err(()); } // TODO: meaningful error messages
      } else { return Err(()); }
    } else { return Err(()); }
  }
}

pub struct HMC5883L {
  bus: LinuxI2CDevice,
  address: u8,
  hard_iron: [i16; 3],
}

impl HMC5883L {
  pub fn new() -> Result<HMC5883L, ()> {
    let mut bus = try!(LinuxI2CDevice::new("/dev/i2c-1", I2C_ADDRESS as u16).map_err(|_|()));
    let mut sensor = HMC5883L { bus: bus, address: I2C_ADDRESS, hard_iron: [0, 0, 0] };
    
    try!(sensor.bus.smbus_write_byte_data(REG_CONF_A, 0x70).map_err(|_|())); // Average 8 samples, 15 Hz output, normal measurement
    try!(sensor.bus.smbus_write_byte_data(REG_CONF_B, 0x20).map_err(|_|())); // 1.3 gain LSb / Gauss 1090 (default)
    try!(sensor.bus.smbus_write_byte_data(REG_MODE,   0x00).map_err(|_|())); // Continuous sampling

    Ok(sensor)
  }

  pub fn with_calibration() -> Result<HMC5883L, ()> {
    let mut sensor = try!(HMC5883L::new());
    sensor.set_hard_iron(try!(MagnetometerCalibration::get_calibration()).hard_iron);
    Ok(sensor)
  }

  pub fn get_axes_raw(&mut self) -> Result<[i16; 3], ()> {
    let x = try!(self.bus.smbus_read_word_data(REG_X).map_err(|_|()));
    let y = try!(self.bus.smbus_read_word_data(REG_Y).map_err(|_|()));
    let z = try!(self.bus.smbus_read_word_data(REG_Z).map_err(|_|()));

    let x = (x>>8) | (x<<8);
    let y = (y>>8) | (y<<8);
    let z = (z>>8) | (z<<8);

    Ok([x as i16 - self.hard_iron[0], y as i16 - self.hard_iron[1], z as i16 - self.hard_iron[2]])
  }

  pub fn set_hard_iron(&mut self, hard_iron: [i16; 3]) {
    self.hard_iron = hard_iron;
  }
}
